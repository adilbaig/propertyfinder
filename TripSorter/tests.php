<?php

include('autoload.php');

/**
 * Run some unit tests
 */

/**
 * Test that passes should have a source and destination
 */
try {
    new BoardingPass\AirportBusPass('', null);
    assert(false, 'This should not happen');
} catch (BoardingPass\BoardingPassException $e) {
    assert($e->getMessage() == 'The source or destination is invalid');
}

/**
 * Test for valid data.
 */
try {
    new BoardingPass\AeroplanePass('SK455', 'Gerona Airport', 'Stockholm', '~~WRONG~~', '3A');
    assert(false, 'This should not happen');
} catch (BoardingPass\BoardingPassException $e) {
    assert($e->getMessage() == "Gate number must be an alphanumeric string");
}

/**
 * Test whether the sorting logic works as expected.
 */
$testColl = new BoardingPassCollection();
$testColl
        ->addBoardingPass(new BoardingPass\AirportBusPass('Madrid', 'Valencia'))
        ->addBoardingPass(new BoardingPass\AirportBusPass('Barcelona', 'Madrid'))
        ->addBoardingPass(new BoardingPass\AirportBusPass('Valencia', 'Seville'));

$testColl->sort();
assert($testColl[0]->getSource() == 'Barcelona');
assert($testColl[0]->getDestination() == 'Madrid');
assert($testColl[1]->getSource() == 'Madrid');
assert($testColl[1]->getDestination() == 'Valencia');
assert($testColl[2]->getSource() == 'Valencia');
assert($testColl[2]->getDestination() == 'Seville');
