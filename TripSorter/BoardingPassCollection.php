<?php

/**
 * BoardingPassCollection 
 * 
 * Stores a collection of boarding passes.
 * Instances are iterable and can access passes using an index.
 */
class BoardingPassCollection implements \IteratorAggregate, \ArrayAccess {

    /**
     * BoardingPasses.
     * 
     * Stores an array of boarding passes. They are not guaranteed to be in sorted order.
     * 
     * @var array
     */
    protected $passes;

    /**
     * Add a BoardingPass.
     * 
     * Add a BoardingPass to this collection. This function presents a fluent interface.
     * 
     * @param BoardingPass\BasicBoardingPass $pass
     * 
     * @return \BoardingPassCollection
     */
    public function addBoardingPass(BoardingPass\BasicBoardingPass $pass) {
        $this->passes[] = $pass;
        return $this;
    }

    /**
     * Sort the boarding passes.
     * 
     * Works by copying object references to a temporary array, while sorting 
     * through the results.
     * 
     * @return void
     */
    public function sort() {
        
        /**
         * Count all locations in one pass ..
         * 
         * Performance : O(n) always
         */
        $locations = $this->getLocationsCount();
        
        /**
         * .. Use this array to get the source and final journey destination. 
         * These are locations that occur only once.
         * 
         * Pop them off the collection so we don't loop through them in the next
         * loop.
         * 
         * Note: We are popping object references, after copying the reference to
         * $sortedPasses. The object itself is not popped or cloned. 
         * 
         * Worst Case Performance : O(n)
         */
        $cnt = count($this->passes);
        $sortedPasses = new \SplFixedArray($cnt);
        foreach($this->passes as $i => $pass) {
            // Get the first source ..
            if($locations[$pass->getSource()] == 1) {
                $sortedPasses[0] = $pass;
                unset($this->passes[$i]);
            } // .. and the final destination .
            elseif($locations[$pass->getDestination()] == 1) {
                $sortedPasses[$cnt - 1] = $pass;
                unset($this->passes[$i]);   
            } 
            
            // If we've found source and destination, break 
            if(!empty($sortedPasses[0]) && !empty($sortedPasses[$cnt - 1])) {
                break;
            }
        }
        
        /**
         * Now we have the original source and final destination.
         * Starting from source get the next boarding pass and loop until all
         * passes have been linked.
         * i.e : Match by checking if the next boarding pass has the same source 
         * as the prior boarding pass's destination. If a match is found the pass
         * is popped off $this->passes.
         * 
         * Best case performance  : O(n)
         * Worst case performance : O(n^2)
         */
        $start = 0;
        while($start < $sortedPasses->count() - 2) {
            $curPass = $sortedPasses[$start];
            
            foreach($this->passes as $i => $pass) {
                //Passes match! 
                if($pass->getSource() == $curPass->getDestination()) {
                    $sortedPasses[$start + 1] = $pass;
                    unset($this->passes[$i]);
                    break;
                }
            }
            
            $start++;
        }
        
        /**
         * Set $this->passes to the final sorted array.
         * Convert to PHP array to keep the data types in sync with the original
         * definition of $this->passes
         */
        $this->passes = $sortedPasses->toArray();
    }
    
    /**
     * Count unique locations across the collection.
     * 
     * Counts the number of times a location has occured as a source or destination.
     * Loops through all passes and generates an associative array in one loop.
     * Performance : O(n) always
     * 
     * @return array - Associative array. Key = Location, Value = no of occurences
     */
    protected function getLocationsCount()
    {
        $places = array();
        foreach ($this->passes as $pass) {
            $set = array($pass->getSource(), $pass->getDestination());

            foreach($set as $k) {
                if(array_key_exists($k, $places)) {
                    $places[$k]++;
                } else {
                    $places[$k] = 1;
                }
            }
        }
        
        return $places;
    }
    
    
    /** --------- INTERFACE IMPLEMENTATIONS ---------  */
    
    /**
     * Implements \IteratorAggregate interface
     * 
     * @return \ArrayIterator
     */
    public function getIterator() {
        return new \ArrayIterator($this->passes);
    }
    
    /*
     * ArrayAccess methods
     */
    
    public function offsetExists($offset) {
        return array_key_exists($offset, $this->passes);
    }

    public function offsetGet($offset) {
        return $this->passes[$offset];
    }

    public function offsetSet($offset, $value) {
        throw new \Exception('This collection is read-only');
    }
    
    public function offsetUnset($offset) {
        throw new \Exception('This collection is read-only');
    }

}