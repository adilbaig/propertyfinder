<?php

include('autoload.php');

/**
 * Create a BoardingPassCollection. It holds boarding passes for a trip.
 * 
 * Insert boarding passes in random order
 */

$coll = new BoardingPassCollection();
$coll
    ->addBoardingPass(new BoardingPass\AirportBusPass('Barcelona', 'Gerona Airport'))
    ->addBoardingPass(new BoardingPass\AirportBusPass('New York JFK', 'Washington'))
    ->addBoardingPass(new BoardingPass\TrainPass('78A', 'Madrid', 'Barcelona', '45B'))
    ->addBoardingPass(new BoardingPass\AeroplanePass('SK22', 'Stockholm', 'New York JFK', '22', '7B'));

$aPass = new BoardingPass\AeroplanePass('SK455', 'Gerona Airport', 'Stockholm', '45B', '3A');
$aPass->setBaggageDropCounter('344');
$coll->addBoardingPass($aPass);

//Now sort the passes once.
$coll->sort();

//Print the passes in order
foreach ($coll as $pass) {
    echo "- ", (string) $pass, "\n";
}

echo "- You have arrived at your final destination", "\n";

