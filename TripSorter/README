Running the Demo
----------------
cd into TripSorter dir and run main.php.

`php main.php`

The output will be a list of sorted boarding passes in order of travel.


Design Discussion
----------------
BoardingPasses all have atleast one thing in common, a source and a destination. I 
created an Abstract base class, BasicBoardingPass, that defines a minimum feature set.
Concrete types of passes are AeroplanePass, AirportBusPass and TrainPass. Each pass
implements the PHP magic method '__toString'. When a pass object is cast to a string
it prints the boarding pass details as an english sentence. 

New passes can be easily created by extending the BasicBoardingPass or SeatedBoardingPass.

The BoardingPassCollection holds a series of boarding passes for a trip. It sorts
the passes before printing. It implements an Iterator interface, so a collection
can be printed in a loop without exposing it's internals or creating an external copy of the 
passes. It also implements the \ArrayAccess class, which is used during unit testing.


Sorting
----------------
Sorting boarding passes is done in two passes. The algorithm is as follows :

1) Count the number of times each location occurs in the collection.
2) Identify the first and last location, they both occur only once.
3) Take the first boarding pass and find the pass whose source matches the first passes destination.
This is the "next" pass.
4) Take the next pass and repeat step 3 until all passes are sorted.

The first step involves looping over the collection of passes, counting occurences 
of each location. We do this in a hash table for efficiency, and readability in PHP.

From this we extract the first and final destination, they are both locations that only
occur once.

Starting from source we loop through the collection looking for a pass whose source 
matches the destination of the first pass. This is our connecting pass. We pop that
off and link it up to our first pass. Now we take the next pass and repeat this loop
over the remaining passes. This process continues until there are no more passes left.

See the code for exact details.

Best Case Performance  : O(n) + O(n) + O(n) = O(n)
Worst Case Performance : O(n) + O(n) + O(n^2) = O(n^2)

This is a simple solution that is easy to understand, but can be optimized. Optimizations include :
1) Do in-place sorting. This is space efficient.
2) A better algorithm that completes sorting in O(n) time. As all boarding passes need to be looked
at for sorting, i:e there is no implicit rules to the data, it is not possible to sort faster than O(n)


Source Code
----------------
All source has been written in OOP style, and documented per PHPDoc specs. Unitests are
available.


Assumptions
----------------
From the requirements doc :

"The implementation of your sorting algorithm should work with any set of boarding passes, 
as long as there is always an unbroken chain between all the legs of the trip. i.e. 
it's one continuous trip with no interruptions."

I have not made any attempts to check if there are gaps in the locations. The purpose of
this code is to show structure, extensibility, testability and documentation.

Circular trips are not handled, ie : A > B > C > A.


Tests
----------------
Unit tests are available in tests.php
Run `php tests.php`