<?php


/**
 * A quick and dirty autoloader
 * 
 * @param string $className
 * 
 */
function __autoload($className) {
    include str_replace('\\', '/', $className) . '.php';
}