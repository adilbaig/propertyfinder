<?php

namespace BoardingPass;

/**
 * BoardingPassException
 * 
 * Is thrown when there is an error in the Boarding Passes
 */
class BoardingPassException extends \Exception {}