<?php

namespace BoardingPass;

/**
 * An Airport Bus pass.
 * 
 * A concrete implementation of a BasicBoardingPass. 
 */
class AirportBusPass extends BasicBoardingPass {

    /**
     * PHP Magic method, used when an object is cast to string
     * 
     * @return string
     */
    public function __toString() {
        return "Take airport bus from {$this->getSource()} to {$this->getDestination()}. No seat assignment.";
    }

}
