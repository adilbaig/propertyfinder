<?php

namespace BoardingPass;

/**
 * A BasicBoardingPass is a pass with a source and destination.
 * 
 * Concrete implementations can add more info.
 */
abstract class BasicBoardingPass {

    /**
     * Store boarding pass details
     * Stored details in an associative array. This makes it simpler for subclasses
     * to add protected details.
     * 
     * @var array 
     */
    protected $dict;

    /**
     * Create a BasicBoardingPass from $source to $destination.
     * Throws an BoardingPassException if source or destination are not valid alpha strings.
     * 
     * @param string $source
     * @param string $destination
     * 
     * @throws BoardingPassException
     */
    public function __construct($source, $destination) {
        if (empty($source) || !is_string($source) || empty($destination) || !is_string($destination)) {
            throw new BoardingPassException("The source or destination is invalid");
        }

        $this->dict['Source'] = trim($source);
        $this->dict['Dest'] = trim($destination);
    }

    /**
     * The Source
     * 
     * @return string
     */
    public function getSource() {
        return $this->dict['Source'];
    }

    /**
     * The Destination
     * 
     * @return string
     */
    public function getDestination() {
        return $this->dict['Dest'];
    }

    /**
     * PHP Magic method, used when an object is cast to string.
     * 
     * Concrete implementations must implement this to output the route and other details
     * as required.
     * 
     * @return string
     */
    abstract public function __toString();
}