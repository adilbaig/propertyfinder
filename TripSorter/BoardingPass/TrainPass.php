<?php

namespace BoardingPass;

/**
 * A concrete implementation of a Train pass.
 */
class TrainPass extends SeatedBoardingPass {

    /**
     * PHP Magic method, used when an object is cast to string
     * 
     * @return string
     */
    public function __toString() {
        return "Take train {$this->getNo()} from {$this->getSource()} to {$this->getDestination()}. Sit in seat {$this->getSeatNo()}.";
    }

}

