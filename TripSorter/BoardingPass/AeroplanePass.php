<?php

namespace BoardingPass;

/**
 * A concrete implementation of an aeroplane pass.
 */
class AeroplanePass extends SeatedBoardingPass {

    /**
     * Ctor
     * 
     * @param string $flightNo - Alphanumeric Flight No.
     * @param string $source - The Source.
     * @param string $destination - The destination.
     * @param string $gateNo - Boarding Gat No. Alphanumeric.
     * @param string $seatNo - Seat No. Alphanumeric.
     * 
     * @throws BoardingPassException
     */
    public function __construct($flightNo, $source, $destination, $gateNo, $seatNo) {
        parent::__construct($flightNo, $source, $destination, $seatNo);

        if (empty($gateNo) || !is_string($gateNo) || !ctype_alnum($gateNo))
            throw new BoardingPassException('Gate number must be an alphanumeric string');

        $this->dict['gateNo'] = $gateNo;
    }

    /**
     * Set the BaggageDrop Counter no.
     * 
     * @param string $counter - Must be an alphanumeric, or a BoardingPassException will be thrown
     * 
     * @return void
     * 
     * @throws BoardingPassException
     */
    public function setBaggageDropCounter($counter) {
        if (empty($counter) || !is_string($counter) || !ctype_alnum($counter))
            throw new BoardingPassException('Baggage counter must be an alphanumeric string');

        $this->dict['bdCntr'] = $counter;
    }

    /**
     * Get BaggageDrop Counter no.
     * 
     * @return string|null
     */
    public function getBaggageDropCounter() {
        return (array_key_exists('bdCntr', $this->dict) ? $this->dict['bdCntr'] : null);
    }

    /**
     * Get the gate no.
     * 
     * @return string
     */
    public function getGateNo() {
        return $this->dict['gateNo'];
    }

    /**
     * PHP Magic method, used when an object is cast to string
     * 
     * @return string
     */
    public function __toString() {
        $bdStr = $this->getBaggageDropCounter() ? "Baggage drop at ticket counter {$this->getBaggageDropCounter()}." : 'Baggage will be automatically transferred from your last leg.';

        return "From {$this->getSource()}, take ﬂight {$this->getNo()} to {$this->getDestination()}. Gate {$this->getGateNo()}, seat {$this->getSeatNo()}. {$bdStr}";
    }

}