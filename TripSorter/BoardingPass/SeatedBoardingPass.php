<?php

namespace BoardingPass;

/**
 * SeatedBoardingPass
 * 
 * A SeatedBoardingPass is a BasicBoardingPass for a reserved mode of transport. 
 */
abstract class SeatedBoardingPass extends BasicBoardingPass {

    /**
     * A SeatedBoardingPass
     * 
     * @param string $no - Alphanum string
     * @param string $source - Source string
     * @param string $destination - Destination string
     * @param string $seatNo - Alphanum string
     */
    public function __construct($no, $source, $destination, $seatNo) {
        parent::__construct($source, $destination);

        if (empty($no) || !is_string($no) || !ctype_alnum($no))
            throw new BoardingPassException('BoardingPass number must be an alphanumeric string');
        
        if (empty($seatNo) || !is_string($seatNo) || !ctype_alnum($seatNo))
            throw new BoardingPassException('Seat number must be an alphanumeric string');
        
        $this->dict['No'] = trim($no);
        $this->dict['SeatNo'] = trim($seatNo);
    }

    /**
     * The boardingPass no.
     * 
     * @return string
     */
    public function getNo() {
        return $this->dict['No'];
    }

    /**
     * The seat no.
     * 
     * @return string
     */
    public function getSeatNo() {
        return $this->dict['SeatNo'];
    }

}

