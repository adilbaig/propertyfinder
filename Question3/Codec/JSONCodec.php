<?php

namespace StringConverter\Codec;

/**
 * This codec converts to/from json formats using json_* functions.
 * 
 * This is a simple encoder. I have only done basic error checking.
 */
class JSONCodec implements ICodec {

    /**
     * Convert $array to a JSON string. 
     * Throws CodecException on failure
     * 
     * @param array $array
     * 
     * @return string
     * 
     * @throws CodecException
     */
    public function encode(array $array) {
        $ret = json_encode($array);
        if($ret === false) {
            throw new CodecException( __CLASS__ . ' could not encode');
        }
        
        return $ret;
    }

    /**
     * Convert $string to a JSON array
     * Throws CodecException on failure
     * 
     * @param string $string
     * 
     * @return array
     * 
     * @throws CodecException
     */
    public function decode($string) {
        $ret = json_decode($string, true);
        if($ret === null) {
            throw new CodecException(__CLASS__ . ' could not decode');
        }
        
        return $ret;
    }

}