<?php

namespace StringConverter\Codec;

/**
 * This codec converts to/from CSV format
 * 
 * This is a very simple codec for demonstration purpose only. A proper
 * encoder would handle, among other things, : 
 * 
 * - multiple lines 
 * - enclose fields so spaces newlines don't break the CSV
 * - use temporary files to handle large data so PHP does not run out of memory
 * - decode strings consistently with or without "'".
 */
class CSVCodec implements ICodec {

    /**
     * Encode $array to a CSV string
     * 
     * @param array $array
     * 
     * @return string
     */
    public function encode(array $array) {
        return implode(',', $array);
    }

    /**
     * Decode $string to an array
     * 
     * @param string $string
     * 
     * @return array
     */
    public function decode($string) {
        return explode(',', $string);
    }

}