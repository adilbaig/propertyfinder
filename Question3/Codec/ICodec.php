<?php

namespace StringConverter\Codec;

/**
 * A generic interface for codecs
 */
interface ICodec {

    /**
     * Encode $array to a string
     * 
     * @param array $array
     * 
     * @return string
     */
    public function encode(array $array);

    /**
     * Decode $string to an array
     * 
     * @param string $string
     * 
     * @return array
     */
    public function decode($string);
}