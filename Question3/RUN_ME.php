<?php

/**
 * ----------------------------------------------------------------
 * Question 3 :
 * 
 * Write a class that will accept a data string and will return back converted
 * version. E.g. you pass it a string which is "json" and the class gives you
 * back "csv" string. The class should be given three parameters: data string,
 * source type and converted type.
 * ----------------------------------------------------------------
 * 
 * Run this file : `cd Question3; php RUN_ME.php`
 * 
 * On success, all asserts will pass. There will be no output.
 */
$converter = new StringConverter\StringConverter();
$str = '{"a":1,"b":2,"c":3,"d":4,"e":5}';
assert($converter->convert($str, 'json', 'csv') == '1,2,3,4,5');
assert($converter->convert('a,2', 'csv', 'json') == '["a","2"]');

try {
    $converter->convert($str, "json", "NONEXISTENT");
    assert(false, 'This should never fire');
} catch (StringConverter\StringConverterException $e) {
    assert($e->getMessage() == "There is no codec for type 'NONEXISTENT'");
}

try {
    // This conversion will fail because 'hello;blah' is not valid json
    $converter->convert('hello;blah', 'json', 'csv');
    assert(false, 'This should never fire');
} catch (StringConverter\Codec\CodecException $e) {
    assert($e->getMessage() == "StringConverter\Codec\JSONCodec could not decode");
}




/**
 * A quick and dirty autoloader
 * 
 * @param string $className
 * 
 */
function __autoload($className) {
    $toks = explode('\\', $className);
    array_shift($toks);
    include implode('/', $toks) . '.php';
}