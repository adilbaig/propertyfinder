<?php

namespace StringConverter;

/**
 * This class converts a string from one format to another.
 * 
 * It works by loading an object of type ICodec, one for $from and $to types.
 * If a codec is not found an exception is thrown. If a codec cannot encode/decode
 * an exception is thrown.
 */
class StringConverter {

    /**
     * Convert a string from $from to $to format
     * 
     * @param string $string - The string to convert
     * @param string $from - The format of the string. 
     * @param string $to - Convert string to $to format. 
     * 
     * @return string
     */
    public function convert($string, $from, $to) {
        $fromCodec = $this->getCodec($from);
        $toCodec = $this->getCodec($to);

        return $toCodec->encode($fromCodec->decode($string));
    }

    /**
     * Loads an ICodec object based on $type
     * 
     * @param int $type
     * 
     * @return \ICodec
     * 
     * @throws StringConverterException
     */
    private function getCodec($type) {

        /**
         * Here we are loading a Codec class using it's name. The advantage is 
         * we can add new Codec classes without modifying our code.
         * 
         * The disadvantage is if we change a class name, say from CSVCodec
         * to CSVAndTSVCodec we have to update user calls everywhere. To prevent
         * this we can define class constants that can be used as a parameter to 
         * getCodec. Ex:
         * 
         * const TYPE_JSON = 1
         * const TYPE_CSV = 2
         * 
         * getCodec(int $type) {
         *  switch($type) {
         *      case self:: TYPE_JSON : //Load JSONCodec
         *      case self:: TYPE_CSV : //Load CSVCodec
         * ...
         * }
         * }
         * 
         */
        $className = 'StringConverter\Codec\\' . strtoupper($type) . 'Codec';
        $fileName = 'Codec/' . strtoupper($type) . "Codec.php";

        if (!file_exists($fileName)) {
            throw new StringConverterException("There is no codec for type '$type'");
        }

        include_once($fileName);
        if (!class_exists($className)) {
            throw new StringConverterException("Class '{$className}' not found in '{$fileName}'");
        }

        return new $className();
    }

}
