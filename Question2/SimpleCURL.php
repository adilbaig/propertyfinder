<?php

namespace SimpleCURL;

include('SimpleCURLException.php');

/**
 * ----------------------------------------------------------------
 * Question 2 :
 * 
 * Write a simple class that "posts" some data to a given URL. This class 
 * must use curl. The user should be able to pass the endpoint and data to be
 * posted.
 * ----------------------------------------------------------------
 */

/**
 * This class calls a URL using php CURL extension
 */
class SimpleCURL {

    /**
     * This function does an HTTP POST request to $url, with $data as payload.
     * 
     * @param string $url - URL to POST
     * @param array $data - An associative array of key value pairs to POST
     * 
     * @return mixed - Result from curl POST
     */
    public static function post($url, array $data) {
        
        //Convert $data to &k=>v form
        $payload = http_build_query($data);
        if(empty($url) || empty($payload)) {
            throw new SimpleCURL\SimpleCURLException('$url or $payload is empty');
        }
        
        //Check the url is valid
        $parsedUrl = parse_url($url);
        if($parsedUrl === false || !array_key_exists('scheme', $parsedUrl) || !array_key_exists('host', $parsedUrl)) {
            throw new SimpleCURL\SimpleCURLException('$url is invalid');
        }
            
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
        //This will return the server's response from curl_exec.
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_ENCODING, 'UTF-8');

        $response = curl_exec($curl);
        if ($response === false) {
            $msg = curl_error($curl);
            $no = curl_errno($curl);
            curl_close($curl);

            // Use a specific exception instead of \Exception
            throw new SimpleCURL\SimpleCURLException($msg, $no);
        }

        curl_close($curl);

        return $response;
    }

}
