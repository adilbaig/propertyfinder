<?php

namespace SimpleCURL;

/**
 * This exception is thrown by SimpleCURL
 */
class SimpleCURLException extends \Exception {
    
}
