<?php

/**
 * ----------------------------------------------------------------
 * Question 1
 * 
 * "Write a singleton class that gives an open mysql connection."
 * ----------------------------------------------------------------
 */

/**
 * Represents a connection handle to the database
 */
class MyDatabaseConnection {

    /**
     * An instance of self
     * 
     * @var self
     */
    private static $instance;

    /**
     * A PDO connection
     * 
     * @var \PDO 
     */
    private $conn;
    
    /**
     * Return self, with a valid connection.
     * 
     * If a connection has been created before, return that. Otherwise initialize 
     * a new connection and return it. 
     * 
     * @return self
     */
    public static function getInstance() {
        if (empty(self::$instance)) {
            /**
             * Connection details can be read from a config file either 
             * manually or using the config class of your framework. Here i am 
             * hard-coding it for demonstration.
             */
            self::$instance = new self('mysql:host=localhost;dbname=test', 'user', 'pass');
        }

        return self::$instance;
    }

    
    /**
     * This constructor inintializes a PDO object. 
     * It is protected so users cannot initialize the object publicly.
     * 
     * Here i have demonstrated a connection to db using PDO. A PDOException will 
     * be thrown on failure. 
     * 
     * @param string $dsn
     * @param string $user
     * @param string $pass
     * 
     * @throws \PDOException
     */
    private function __construct($dsn, $user, $pass) {
        $this->conn = new \PDO($dsn, $user, $pass);
    }

}
